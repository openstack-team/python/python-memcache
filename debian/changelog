python-memcache (1.62-3) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090531).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 10:00:39 +0100

python-memcache (1.62-2) unstable; urgency=medium

  * Removed python3-mock from build-depends.

 -- Thomas Goirand <zigo@debian.org>  Tue, 24 Sep 2024 10:07:00 +0200

python-memcache (1.62-1) unstable; urgency=medium

  * d/watch: switch to using mode=git.
  * New upstream release:
    - Removes python3-six depends (Closes: #1070355).
  * Drop python3.8.diff applied upstream.
  * Drop python312-remove-a-spurious-backslash.patch applied upstream.
  * Fix using-port-11212-instead-of-standard-11211.patch.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 May 2024 08:04:59 +0200

python-memcache (1.59-8) unstable; urgency=medium

  * Fix invalid escape sequence '\ ' with python 3.12 by applying upstream
    patch. Thanks to Vladimir Petko for the debdiff (Closes: #1063487).

 -- Thomas Goirand <zigo@debian.org>  Fri, 09 Feb 2024 08:40:20 +0100

python-memcache (1.59-7) unstable; urgency=medium

  * Cleans better (Closes: #1046789).

 -- Thomas Goirand <zigo@debian.org>  Fri, 18 Aug 2023 10:33:38 +0200

python-memcache (1.59-6) unstable; urgency=medium

  * Use pytest instead of nose (Closes: #1018532).
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Mon, 14 Aug 2023 09:30:12 +0200

python-memcache (1.59-5) unstable; urgency=medium

  * Add Restrictions: superficial to d/tests/control (Closes: #974468).
  * Switch to debhelper-compat 11.

 -- Thomas Goirand <zigo@debian.org>  Sat, 21 Nov 2020 00:17:20 +0100

python-memcache (1.59-4) unstable; urgency=medium

  * Team upload.
  * Fix comparisons with literals (Patch from Ubuntu, closes: #950209).
  * Bump Standards-Version to 4.5.0 (no changes).
  * d/control: Add Rules-Requires-Root: no.

 -- Ondřej Nový <onovy@debian.org>  Thu, 30 Jan 2020 10:47:55 +0100

python-memcache (1.59-3) unstable; urgency=medium

  * Removed unit test using Python 2.

 -- Thomas Goirand <zigo@debian.org>  Thu, 07 Nov 2019 21:25:22 +0100

python-memcache (1.59-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Removed Python 2 support (Closes: #937914).

 -- Thomas Goirand <zigo@debian.org>  Sat, 26 Oct 2019 00:49:45 +0200

python-memcache (1.59-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.7 now (no change).
  * Added Debian tests.
  * Standards-Version is 3.9.8 now (no change)
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Daniel Baumann ]
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.1.0.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces

  [ Thomas Goirand ]
  * New upstream release:
    - Fix Storing non-ascii values in memcache fails on Py 2 (Closes: #894773).
  * Removed More_Python_3_fixes.patch, kind of applied upstream.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 06 Sep 2018 01:18:05 +0200

python-memcache (1.57-1) unstable; urgency=medium

  * New upstream release.
  * Moving the package to PKG OpenStack as I don't have write access to the
    DPMT repository anymore.
  * Switch the homepage to the Github URL.
  * Switch the watch file to use the github tags, as the upstream FTP is never
    updated.
  * Added More_Python_3_fixes.patch.
  * Cleans setup-test-env-memcached.sh from unused function wait_for_line.
  * Testing with all versions of Python(3).
  * Fixed debian/copyright ordering.

 -- Thomas Goirand <zigo@debian.org>  Fri, 05 Feb 2016 08:12:59 +0000

python-memcache (1.56-1) unstable; urgency=medium

  * New upstream release:
    - Full upstream Python 3 support: deleting debian patches applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Mon, 27 Jul 2015 13:23:00 +0000

python-memcache (1.54+20150423+git+48e882719c-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2015 20:59:58 +0000

python-memcache (1.54+20150423+git+48e882719c-1) experimental; urgency=medium

  * Added patch from Victor Stinner to port the package to Python 3.
  * Now runs unit tests when building the package:
    - Build-Depends: memcached, nose, six.
    - Added nose as build-depends, and run unit tests.
    - Added debian/setup-test-env-memcached.sh to start memcached on
      non-standard port (which is compatible with building with sbuild).
  * Standards-Version: 3.9.6.
  * Added dh-python as build-depends.

 -- Thomas Goirand <zigo@debian.org>  Thu, 23 Apr 2015 21:47:37 +0000

python-memcache (1.53+2014.06.08.git.918e88c496-1) unstable; urgency=medium

  * New upstream release.
  * Removed now useless X-Python-Version: >= 2.6.
  * Upgraded to debhelper and compat 9.
  * Added Python3 support.
  * debian/copyright file now using parseable format 1.0.
  * Added myself as uploader.

 -- Thomas Goirand <zigo@debian.org>  Mon, 09 Jun 2014 09:25:03 +0000

python-memcache (1.53-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Carl Chenet ]
  * New upstream release
  * debian/control
    - bump Standards-Version to 3.9.4.0
    - use python-all in B-D

 -- Carl Chenet <chaica@ohmytux.com>  Wed, 19 Jun 2013 23:02:23 +0200

python-memcache (1.48-1) unstable; urgency=low

  * New upstream release
  * debian/control
    - bump X-Python-Version >= 2.6
    - bump Standards-Version to 3.9.2.0
    - removed ${python:Breaks} to binary package info
  * debian/rules
    - add override_dh_auto_build to remove Makefile

 -- Carl Chenet <chaica@ohmytux.com>  Wed, 07 Dec 2011 23:30:42 +0100

python-memcache (1.47-1) unstable; urgency=low

  * New upstream release. (Closes: #609908)
  * debian/source/format
    - Bump to 3.0 (quilt)
  * debian/control
    - Removed python-support from Build-Depends-Indep
    - Bump python to 2.6.6-3
    - Bump Standards-Version to 3.9.1.0.
    - Replaced XS-Python-Version: all by X-Python-Version >= 2.5
    - Added Breaks: ${python:Breaks} to binary package info
  * debian/copyright
    - Removed path to BSD license and include the license text itself
  * debian/rules
    - Replaced dh $@ by dh $@ --with python2

 -- Carl Chenet <chaica@ohmytux.com>  Thu, 17 Feb 2011 01:30:02 +0100

python-memcache (1.45-1) unstable; urgency=low

  * New upstream version.
  * debian/control
    - Switched the Maintainer field to DPMT.
    - Switched the Uploader field to Carl Chenet <chaica@ohmytux.com>.
    - Bump the Standards-Version field to 3.8.4

 -- Carl Chenet <chaica@ohmytux.com>  Tue, 02 Feb 2010 20:42:02 +0100

python-memcache (1.44-1) unstable; urgency=low

  * New upstream version.
  * debian/control
    - bump Standards-Version to 3.8.3
    - bump required python version.
  * debian/watch
    - added the pasv option.
  * debian/copyright
    - Modified (C) to ©.
  * debian rules
    - Using minimal dh7 now.
  * debian/compat
    - Bump to 7.

 -- Carl Chenet <chaica@ohmytux.com>  Tue, 22 Sep 2009 01:20:31 +0200

python-memcache (1.40-2) unstable; urgency=medium

  * debian/control
    - switch Vcs-Browser field to viewsvn
    - bump Standards-Version to 3.8.1 (no changes needed)
  * debian/watch
    - fixed (upstream ftp site uses symlinks, breaking version check)
  * debian/rules
    - fixed for the new python-support; thanks to Josselin Mouette for the
      report and patch; Closes: #517063

 -- Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>  Fri, 10 Apr 2009 12:58:57 +0200

python-memcache (1.40-1) unstable; urgency=low

  * Initial release. (Closes: #458608)

 -- Christopher Schmidt <crschmidt@metacarta.com>  Thu,  3 Jan 2008 18:47:00 -0500
